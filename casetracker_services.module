<?php

/**
 * Implementation of hook_menu().
 */
function casetracker_services_menu($may_cache) {
  if ($may_cache) {
    $items[] = array(
      'path'               => 'admin/settings/casetracker_services',
      'access'             => user_access('administer case tracker'),
      'title'              => 'Case Tracker services',
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('casetracker_services_settings'),
    );
  }
  return $items;
}


/**
 * Configures the various Case Tracker Server options.
 * 
 * The main settings simply take each api key generate in the services
 * module and ask the user to allocate a valid user.
 *
 * This way, when the request comes in, the user has already been validated
 * by the services module and we protect the system from changes outside
 * the validated project.
 *
 */
function casetracker_services_settings() {
  $form = array();

  $form['casetracker_services'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Project access'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#description'   => t('Assign projects to all the remote websites set up in the Services admin'),
  );
  
  $mappings = casetracker_services_project_mapping_load();

  $projects = db_query("SELECT nid, title FROM {node} WHERE type = 'casetracker_basic_project'");
  $project_choices[0] = 'Blocked';
  while ($project = db_fetch_array($projects)) {
    $project_choices[$project['nid']] = $project['title'];
  }
  foreach ($mappings as $key => $data) {
    $form['casetracker_services'][$key] = array(
      '#type'              => 'select',
      '#title'             => $data['domain'],
      '#description'       => $data['title'],
      '#options'           => $project_choices,
      '#default_value'     => $data['project'],
    );
  }

  $form['submit'] = array(
    '#type'              => 'submit', 
    '#value'             => t('Submit'), // this text is an easter egg.
  );
    
  return $form;
}

/**
 * Find out the key that the user is connecting by.
 * Now this only works because we add the key to the url call.
 * There should be a way to get this key from the services module but
 * I don't know a way
 */
function casetracker_services_get_key() {
 // Assume the key is arg 2... 
  if (!arg(2)) {
    watchdog('A call for Case Tracker Server from is missing the required api key in the third part of the url.');
    return false;
  }

 return arg(2);
}

function casetracker_services_settings_submit($form_id, $form_values) {
  casetracker_services_project_mapping_save($form_values);
}


function casetracker_services_get_project() {
  $key = casetracker_services_get_key();
  if (!$key) {
    return;
  }
  return casetracker_services_project_mapping_load($key);
}

function casetracker_services_project_mapping_load($key = false) {
  $mappings = unserialize(variable_get('casetracker_services_project_mapping', ''));

  $keys = services_get_keys();
  if ($key) {
    // A service call, just return the allowed project.
    return $mappings[$key];
  }
  foreach ($keys as $kid => $data) {
    $mappings[$kid] = array('project' => (isset($mappings[$kid]) ? $mappings[$kid] : 0));
    $mappings[$kid]['title'] = $data->title;
    $mappings[$kid]['domain'] = $data->domain;
  }
  return $mappings;  
}

/**
 * Save the project id/services key mapping. Called by settings page.
 */
function casetracker_services_project_mapping_save($form_values = array()) {
  
  $keys = services_get_keys();
  $mappings = array();
  foreach ($keys as $kid => $data) {
    if (isset($form_values[$kid])) {
      $mappings[$kid] = $form_values[$kid];
    }
  }
  
  variable_set('casetracker_services_project_mapping', serialize($mappings));
}

/**
 * Implementation of hook_service()
 */
function casetracker_services_service() {
  return array(    
    // casetracker.load
    array(
      '#method'   => 'casetracker.node',
      '#callback' => 'casetracker_services_node',
      '#args'     => array(
        array(
          '#name'         => 'case',
          '#type'         => 'string',
          '#description'  => t('A case nid.'))
        ),
      '#return'   => 'struct',
      '#help'     => t('Returns a Casetracker Case.'),
    ),
    array(
      '#method'   => 'casetracker.cases',
      '#callback' => 'casetracker_services_cases',
      '#args'     => array(
        array(
          '#name'         => 'filter',
          '#type'         => 'string',
          '#description'  => t("Valid case tracker filter, project will be applied either way."),
        ),
      ),
      '#return'   => 'struct',
      '#help'     => t('Returns a list of casetracker cases.'),
    ),
    array(
      '#method'   => 'casetracker.log',
      '#callback' => 'casetracker_services_log',
      '#args'     => array(
        array(
          '#name'         => 'nid', 
          '#type'         => 'array',
          '#description'  => t("'new' for a new case, otherwise a nid."),
        ),
        array(
          '#name'         => 'data',
          '#type'         => 'array',
          '#description'  => t("The values required are: 'body', 'title', 'status', 'case_priority_id', 'case_status_id', 'case_type_id'"),
        ),
      ),
      '#return'   => 'struct',
      '#help'     => t('Creates a new case.'),
    ),
    array(
      '#method'   => 'casetracker.codes',
      '#callback' => 'casetracker_services_codes',
      '#return'   => 'struct',
      '#help'     => t('Returns status and type codes.'),
    ),
  );
}

/**
 * Returns a specified node.
 */
function casetracker_services_node($nid) {
  $node = node_load($nid);
  if ($node->pid != casetracker_services_get_project()) {
    return t("Accessed denied");
  }
  
  $comments = comment_render($node);
  $node = (array)node_build_content($node, $teaser, $page);
  $node['comments'] = $comments;
  return $node;
}

/**
 * Returns a list of cases.
 */
function casetracker_services_cases($filter) {
  $project = casetracker_services_get_project();

  $return = array();
  $rows = array();

  // Would be nice to be able to get the result set from casetracker_cases_overview(),
  // instead we completely ignore that monster and just use some similar SQL.
  $query = "SELECT DISTINCT(n.nid), n.title, ncs.last_comment_timestamp, cc.case_number, cc.case_priority_id, cc.case_status_id, cc.case_type_id, cc.assign_to, cp.project_number FROM node n LEFT JOIN casetracker_case cc ON (n.vid = cc.vid) LEFT JOIN casetracker_project cp ON (cp.nid = cc.pid) LEFT JOIN node_comment_statistics ncs ON (n.nid = ncs.nid) WHERE n.status = 1 AND n.type IN ('casetracker_basic_case') AND cc.pid IN (%d) ORDER BY ncs.last_comment_timestamp DESC LIMIT 0, 100";
  $result = db_query($query, $project);
  while ($row = db_fetch_array($result)) {
    $rows[] = $row;
  }
  $output['data'] = $rows;

  $lookup = array();
  $query = "SELECT csid, case_state_name FROM {casetracker_case_states}";

  $result = db_query($query);
  while ($row = db_fetch_array($result)) {
    $lookup[$row['csid']] = $row['case_state_name'];
  }
  $output['lookup'] = $lookup;

  return $output;
}

/**
 * Returns a list of CT codes
 */
function casetracker_services_codes() {
  $project = casetracker_services_get_project();

  $lookup = array();
  $query = "SELECT csid, case_state_realm, case_state_name FROM {casetracker_case_states}";

  $result = db_query($query);
  while ($row = db_fetch_array($result)) {
    $lookup[$row['case_state_realm']][$row['csid']] = $row['case_state_name'];
  }

  return $lookup;
}


/**
 * Create a case, or add a comment, depending on the presense of a nid.
 */
function casetracker_services_log($nid, $data) {  
  $project = casetracker_services_get_project();
  if (!$project){
    return;
  }
  $project = node_load($project);
  $uid = $project->uid;


  if ($nid == 'new') {  
    $data = (object)$data;
    $data->type = 'casetracker_basic_case';
    $data->uid = $project->uid;
    $data->pid = $project->nid;
    $data->status = 1;
    $data->comment = 2;
    $data->promote = 0;
    node_save($data);
    return $data->nid;
  }
  elseif (is_numeric($nid)) {
    $node = node_load($nid);
    $account = user_load(array('uid' => $uid));
    global $user;
    $user = $account;

    if ($node->pid == $project->nid) {
      $comment = array();
      $comment['nid'] = $nid;
      $comment['revision_id'] = $node->vid;
      $comment['prid'] = $project->nid;
      $comment['author'] = $user->name;
      $comment['case_type_id'] = $data['case_type_id'];
      $comment['case_status_id'] = $data['case_status_id'];
      $comment['case_priority_id'] = $data['case_priority_id'];
      $comment['subject'] = $data['title'];
      $comment['comment'] = $data['body'];
      comment_form_validate('comment_form', $comment);
      if ($errors = form_get_errors()) {
        watchdog('ctserver', "Errors were generated updating a case from ". $data->url .": ". print_r($errors, true));
      }
      $comment['case_title'] = $node->title; // We're not currently doing remote case title changes.
      comment_form_submit('comment_form', $comment);
      return $nid;
    }
  }

  return false;
}

function db_queryct($query) {
  $args = func_get_args();
  array_shift($args);
  $query = db_prefix_tables($query);
  if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
    $args = $args[0];
  }
  _db_query_callback($args, TRUE);
  $query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
  watchdog('ct', $query);
}